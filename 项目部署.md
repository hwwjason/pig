### [#](https://pig4cloud.com/doc/pig#特别说明) 特别说明

- [pig4cloud 基础环境软件下载（windows 版本）](https://pan.baidu.com/s/1uuxy3o2AzvBPceu6bjeRGA)
- [基础软件安装视频](https://www.bilibili.com/video/av39687329?p=14)
- 完全使用本教程的操作方法，减少踩坑时间，不要做莽夫！
- 建议使用 IDEA 2019
- 确保已经安装 [Lombok Plugin](https://plugins.jetbrains.com/plugin/6317-lombok-plugin)

### [#](https://pig4cloud.com/doc/pig#v2.6-部署视频) v2.6 部署视频

[pig v2.6 部署视频 点击查看 公众号欢迎关注](https://mp.weixin.qq.com/s?__biz=MjM5MzEwODY4Mw==&mid=2257484108&idx=1&sn=2a6f56c0717e5f8c8530c96453916499&chksm=a5e6dee8929157fef98af6d67f384b4be070133677df0efcf6d6aecc780af9a1e7806c637370&token=2064230332&lang=zh_CN#rd)

### [#](https://pig4cloud.com/doc/pig#环境说明) 环境说明

| 中间件 |  版本   |                            备注                             |
| :----: | :-----: | :---------------------------------------------------------: |
|  JDK   |   1.8   |       强制要求,1.8以上版本请自行添加Java EE相关jar包        |
| MySQL  | 5.7.8 + |             强制要求,至少5.7!当然8.0也没有问题              |
| Redis  |  3.2 +  | windows版只能使用Redis3.2,类Unix系统使用最新的5.0也没有关系 |
|  node  | 10.0 +  |                    LTS版本，不要使用12.X                    |
|  npm   |  6.0 +  |                                                             |
| maven  |  3.5+   |                                                             |

### [#](https://pig4cloud.com/doc/pig#一、项目下载) 一、项目下载

```
git clone https://gitee.com/log4j/pig.git
```

### [#](https://pig4cloud.com/doc/pig#二、配置本地hosts) 二、配置本地hosts

[win配置方法](https://www.jb51.net/os/win10/395409.html) | [mac配置方法](https://www.jianshu.com/p/752211238c1b) | 建议使用 switchhost，开源群下载,对自己的网络环境自信的朋友，也可以直接[官网](https://github.com/oldj/SwitchHosts/releases)下载

```
# 本地测试环境  
127.0.0.1   pig-mysql
127.0.0.1   pig-redis
127.0.0.1   pig-gateway
127.0.0.1   pig-register
```

### [#](https://pig4cloud.com/doc/pig#三、初始化数据库) 三、初始化数据库

- 参数说明

```
版本： mysql5.7.8+
默认字符集: utf8mb4
默认排序规则: utf8mb4_general_ci
```

- 脚本说明

```
pig/db/pig.sql
pig/db/pig_config.sql
```

### [#](https://pig4cloud.com/doc/pig#四、pig配置修改) 四、pig配置修改

**特别说明: host配置不要改成IP**

- `nacos`数据库源信息修改
  pig/pig-register/src/main/resources/bootstrap.yml

```
db:
  num: 1
  user: ${MYSQL-USER:root}  #修改:用户名
  password: ${MYSQL-PWD:root} #修改:密码
  url:
    0: jdbc:mysql://${MYSQL-HOST:pig-mysql}:${MYSQL-PORT:3306}/${MYSQL-DB:pig_config}?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&nullCatalogMeansCurrent=true&allowPublicKeyRetrieval=true
```

- redis、MySQL 配置

启动 `PigNacosApplication` , 访问： http://pig-register:8848/nacos (默认账号密码 nacos/nacos) ![img](项目部署.assets/20191129204108_1ySkqR_Screenshot.jpeg)

application-dev.yml

```
# redis 相关，无密码为空即可，不要修改成IP,修改hosts
spring:
  redis:
    password:
```

- 数据库密码配置,修改以下几个文件

```
pig-auth-dev.yml    
pig-upms-dev.yml  
pig-codegen-dev.yml
# 数据源,只需要修改密码即可，不要修改成IP,修改hosts
spring:
  datasource:
    username: root
    password: lengleng
```

### [#](https://pig4cloud.com/doc/pig#五、启动顺序) 五、启动顺序

```
1. PigNacosApplication  
2. PigGatewayApplication  
3. PigAuthApplication 
4. PigAdminApplication  
```

- 使用代码生成、监控时再启动以下项目

```
5. PigCodeGenApplication  
6. PigMonitorApplication  
```

### [#](https://pig4cloud.com/doc/pig#六、启动前端) 六、启动前端

- 项目下载

```
git clone https://gitee.com/log4j/pig-ui.git
```

- 安装cnpm 代理 npm install -g cnpm --registry=https://registry.npm.taobao.org
- 安装依赖 cnpm install
- 启动 npm run dev